------------------------------ MODULE knapsack ------------------------------

EXTENDS TLC, Integers
PT == INSTANCE PT

CONSTANTS Capacity, Items, SizeRange, ValueRange
ASSUME SizeRange \subseteq 1..Capacity
ASSUME Capacity > 0
ASSUME \A v \in ValueRange: v >= 0

ItemParams == [size: SizeRange, value: ValueRange]
ItemSets == [Items -> ItemParams]

KnapsackSize(sack, itemset) ==
    LET size_for(item) == itemset[item].size * sack[item]
    IN PT!ReduceSet(LAMBDA item, acc: size_for(item) + acc, Items, 0)
    
ValidKnapsacks(itemset) ==
    {sack \in [Items -> 0..4]: KnapsackSize(sack, itemset) <= Capacity}
    
KnapsackValue(sack, itemset) ==
    LET value_for(item) == itemset[item].value * sack[item]
    IN PT!ReduceSet(LAMBDA item, acc: value_for(item) + acc, Items, 0)
    
BestKnapsack(itemset) ==
    LET all == ValidKnapsacks(itemset)
    IN CHOOSE best \in all:
        \A worse \in all \ {best}:
        KnapsackValue(best, itemset) > KnapsackValue(worse, itemset)
        
BestKnapsacksOne(itemset) ==
    LET all == ValidKnapsacks(itemset)
    IN
        CHOOSE all_the_best \in SUBSET all:
            \E good \in all_the_best:
                /\ \A other \in all_the_best:
                    KnapsackValue(good, itemset) = KnapsackValue(other, itemset)
                /\ \A worse \in all \ all_the_best:
                    KnapsackValue(good, itemset) > KnapsackValue(worse, itemset)

BestKnapsacksTwo(itemset) ==
    LET value(sack) == KnapsackValue(sack, itemset)
        all == ValidKnapsacks(itemset)
        best == CHOOSE best \in all:
            \A worse \in all \ {best}:
                value(best) >= value(worse)
        value_of_best == value(best)
    IN
        {k \in all: value_of_best = value(k)}

(*--algorithm debug 
variables itemset \in ItemSets
begin
    assert BestKnapsacksTwo(itemset) \subseteq ValidKnapsacks(itemset);
end algorithm;*)
\* BEGIN TRANSLATION
VARIABLES itemset, pc

vars == << itemset, pc >>

Init == (* Global variables *)
        /\ itemset \in ItemSets
        /\ pc = "Lbl_1"

Lbl_1 == /\ pc = "Lbl_1"
         /\ Assert(BestKnapsacksTwo(itemset) \subseteq ValidKnapsacks(itemset), 
                   "Failure of assertion at line 54, column 5.")
         /\ pc' = "Done"
         /\ UNCHANGED itemset

Next == Lbl_1
           \/ (* Disjunct to prevent deadlock on termination *)
              (pc = "Done" /\ UNCHANGED vars)

Spec == Init /\ [][Next]_vars

Termination == <>(pc = "Done")

\* END TRANSLATION
        
=============================================================================
\* Modification History
\* Last modified Fri Oct 26 15:11:13 EDT 2018 by amyers
\* Created Tue Oct 23 11:19:05 EDT 2018 by amyers
