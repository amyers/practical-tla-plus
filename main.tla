-------------------------------- MODULE main --------------------------------

EXTENDS TLC, Integers, FiniteSets, Sequences

CONSTANTS Nodes, NULL

INSTANCE LinkedLists WITH NULL <- NULL

AllLinkedLists == LinkedLists(Nodes)

CycleImpliesTwoParents(ll) == 
    Cyclic(ll) <=> Ring(ll) \/ \E x, y \in DOMAIN ll: ll[x] = ll[y] /\ x /= y

Valid ==
    /\ \A ll \in AllLinkedLists:
        /\ Assert(CycleImpliesTwoParents(ll), <<"Counterexample:", ll>>)

=============================================================================
\* Modification History
\* Last modified Fri Nov 09 18:27:58 EST 2018 by amyers
\* Created Tue Nov 06 12:40:40 EST 2018 by amyers
